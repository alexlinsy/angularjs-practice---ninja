var app = angular.module("myApp", ['ngRoute', 'ngAnimate']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

  $locationProvider.html5Mode(true);
  
  $routeProvider
    .when('/home', {
      templateUrl:'../content/html/home.html'
    })
    .when('/directory', {
      templateUrl:'../content/html/showNinjas.html',
      controller:'ninjasCtrl'
    }).otherwise({
      redirectTo:'/home'
    });

}]);


app.directive('ninjasList', function() {
  return {
    restrict: 'A',
    templateUrl: '../content/html/ninjasList.html',
    scope: {
      ninja: '=',
      ninjas: '='
    },
    controller: function($scope) {
      $scope.removeNinja = function (ninjia) {
        console.log($scope.ninjas);
        let indexOfNinja = $scope.ninjas.indexOf(ninjia);
        $scope.ninjas.splice(indexOfNinja, 1);
      }
    },
    replace: true
  }
});

app.directive('ninjaHeader', function() {
  return {
    restrict: 'E',
    templateUrl: '../content/html/ninjaHeader.html',
    replace: true
  }
});

app.service('ninjaService', function($http) {
  this.getNinja = function() {
    return $http.get('../content/json/ninjas.json');
  }
});

app.controller('ninjasCtrl', ['$scope', '$http', 'ninjaService', function($scope, $http, ninjaService) {

  let promise = ninjaService.getNinja();
  promise.then(function success(response) {
    $scope.ninjas = response.data;
  }, function error(response) {
    alert("Retrieved data failed");
    console.log("Retrieved data failed");
  });
  $scope.addNinja = function() {
    var newninja = {
      name: $scope.newninja.name,
      belt: $scope.newninja.belt,
      rate: parseInt($scope.newninja.rate),
      available: true
    };
    $scope.ninjas.push(newninja);
  }

}]);
